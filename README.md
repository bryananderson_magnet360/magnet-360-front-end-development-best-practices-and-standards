# README #

This repository holds the Front End Development Best Practices and Guidelines for Magnet 360 Developers and the Salesforce Platform.

### What is this repository for? ###

* This repository holds documentation for best practices and standards for Front End Deveopment at Magnet 360.

### How do I get set up? ###

* Please ask Bryan Anderson for access to this repository.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Bryan Anderson (bryan.anderson@magnet360.com)

### Is there something that you want to add or update? ###

* Create an issue under "Issues" and assign to Bryan Anderson
* Set "Kind" to "Enhancement" for change and "Proposal" for something new
* Set "Priority" to "Major" by default
* Approval or discussion can take place in #frontendpractice channel or during the bi-weekly Front End Practice meetings.
